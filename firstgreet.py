# -*- coding: utf8 -*-
"""
firstgreet.py - Willie simple first time Greater Module
Copyright 2008, Sean B. Palmer, inamidst.com
Copyright © 2012, Elad Alfassa <elad@fedoraproject.org>
Licensed under the Eiffel Forum License 2.

http://willie.dftba.net
"""

import time
import os.path
#from willie.tools import Ddict

########### settings ### ##############
messages=[
	u'Welcome to #ubuntu-il! Feel free to ask questions. Note that it may take at least around half an hour until you get a response. Enjoy!',
	u'ברוכים הבאים ל #ubuntu-il! הרגישו חופשי לשאול שאלות. שימו לב שעלולה לקחת לפחות כחצי שעה לקבלת מענה. תהנו!',
	]  #messages are sent together on each new user
save_every=1 #after how many new nicks do you want to save?
seenfile=os.path.join(os.path.expanduser('~'),os.path.normpath('.willie/seen.txt'))
########### settings ### ##############

counter=0
seen_dict=[]

def firstgreet(willie, trigger):
	nick = trigger.nick.lower()
	while nick[-1]== '_': #remove trailing underscores
		nick=nick[:-1]
	#print nick, seen_dict
	if nick not in seen_dict:
		global counter
		counter+=1
		seen_dict.append(nick)
		for msg in messages:
			willie.say(str(trigger.nick) + ': ' + msg)
		if counter%save_every==0:
			try:
				seenf=open(seenfile,'a')
				for nick in seen_dict[-save_every:]:
					seenf.write(nick+'\n')
				seenf.close()
			except:
				print "Unable to open %s in append mode. maybe you need an empty file there? if you can't solve it, go bug Avihay\n"
firstgreet.rule = r'(.*)'
firstgreet.event = 'JOIN'
firstgreet.priority = 'low'

def setup(willie):
	try:
		seenf=open(seenfile,'r')
		for line in seenf:
			seen_dict.append(line.strip())
		seenf.close()
	except:
		print "Unable to open %s in read mode. This is normal for a first run\n"
		pass
	print 'Setup complete for firstgreet\n'
	print seen_dict

if __name__ == '__main__':
    print __doc__.strip()
